class Menu {
  constructor() {
    let menu = document.querySelector('.menu-mobile');
    let nav = document.querySelector('.site-nav');

    menu.addEventListener('click', (e) => {
      e.stopPropagation();
      e.preventDefault();
      nav.classList.toggle("active");
    });

    document.addEventListener('click', (e) => {
      nav.classList.remove("active");
    });
  }
}
