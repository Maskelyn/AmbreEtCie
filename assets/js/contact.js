class Contact {
  constructor() {
    let contactForm = document.querySelector('.form-contact');
    if (contactForm) {
      var action = contactForm.getAttribute('data-tmp-action');
      contactForm.setAttribute('action', action);

      // set the value of the redirection page
      contactForm.querySelector('.input-redirect').setAttribute('value', document.location.href.replace('contact', 'confirmation'));

    }
  }
}
