---
name: Fromage
titre : Cat's Cheese
description: Jouet pour chat couverture en peluche, avec souris peluche sur tige et 3 balles.
image: "/assets/img/boutique/Jouets/fromage.jpg"
order: 6
---
