---
name: Dog disque
titre : Flash Dog Disc
description: Disque lumineux de 20 cm, en caoutchouc thermoplastique, solide et durable.
image: "/assets/img/boutique/Jouets/dog-disque.jpeg"
order: 23
---
