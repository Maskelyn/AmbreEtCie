---
name: Baguette bleu   
titre : Canne à pêche avec oiseau verte
description: Canne à pêche avec oiseau en tissu avec clochette et bande en peluche, suspendu sur un élastique.
image: "/assets/img/boutique/Jouets/baguette-bleu.jpg"
order: 8
---
