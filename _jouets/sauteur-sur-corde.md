---
name: Sauteur sur corde
titre : Sporting
description: Sauteur sur corde en caoutchouc naturel et polyester, très robuste, avec poignée.
image: "/assets/img/boutique/Jouets/sauteur-sur-corde.jpg"
order: 21
---
