---
name: Une souris verte
titre : Souris peluche
description: Une souris verte et blanche en peluche pour chat.
image: "/assets/img/boutique/Jouets/une-souris-verte.jpg"
order: 13
---
