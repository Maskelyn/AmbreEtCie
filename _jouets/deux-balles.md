---
name: Deux balles
titre : Lot de 2 balles pour chats
description: Set de balles en plastique à cliquetis avec queue.
image: "/assets/img/boutique/Jouets/deux-balles.jpg"
order: 10
---
