---
name: Tour a chat rouge
titre : Nevio Cat Tower
description: Tour pour chat recouverte de peluche, avec panneau en sisal et jouets suspendus. 3 coussins réversibles, lavables en machine.
image: "/assets/img/boutique/Jouets/tour-a-chat-rouge.jpg"
order: 3
---
