---
name: Baguette rose   
titre : Canne à pêche avec oiseau rouge
description: Canne à pêche avec oiseau en tissu avec clochette et bande en peluche, suspendu sur un élastique.
image: "/assets/img/boutique/Jouets/baguette-rose.jpg"
order: 7
---
