---
name: Snack snake
titre : Snack snake
description: Serpent en caoutchouc thermoplastique de 42 cm, avec fente pour friandises. Fait du son.
image: "/assets/img/boutique/Jouets/snack-snake.jpg"
order: 19
---
