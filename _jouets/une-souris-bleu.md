---
name: Une souris bleu
titre : Souris peluche
description: Une souris bleue et blanche en peluche pour chat.
image: "/assets/img/boutique/Jouets/une-souris-bleu.jpg"
order: 12
---
