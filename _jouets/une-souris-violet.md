---
name: Une souris violet
titre : Souris peluche
description: Une souris violette et blanche en peluche pour chat.
image: "/assets/img/boutique/Jouets/une-souris-violet.jpg"
order: 15
---
