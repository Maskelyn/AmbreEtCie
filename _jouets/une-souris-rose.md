---
name: Une souris rose
titre : Souris peluche
description: Une souris rose et blanche en peluche pour chat.
image: "/assets/img/boutique/Jouets/une-souris-rose.jpg"
order: 14
---
