---
name: Balle sur corde
titre : Sporting
description: Balle hérisson en caoutchouc termoplastique sur corde. Solide et durable.
image: "/assets/img/boutique/Jouets/balle-sur-corde.jpg"
order: 22
---
