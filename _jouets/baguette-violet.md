---
name: Baguette violet  
titre : Canne à pêche avec oiseau violette
description: Canne à pêche avec oiseau en tissu avec clochette et bande en peluche, suspendu sur un élastique.
image: "/assets/img/boutique/Jouets/baguette-violet.jpg"
order: 9
---
