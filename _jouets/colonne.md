---
name: Colonne
titre : Rouleau de jeu chat
description: Rouleau en sisal avec 2 jouets, à suspendre ou à mettre a plat.
image: "/assets/img/boutique/Jouets/colonne.jpg"
order: 4
---
