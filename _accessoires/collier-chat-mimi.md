---
name: Collier chat Mimi
titre : Mimi
description: Collier pour chats Mimi réglable, avec clochettes. Disponibles en plusieurs couleurs.
image: "/assets/img/boutique/Accessoires/collier-chat-mimi.jpg"
order: 6
---
