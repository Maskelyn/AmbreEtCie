---
name:  Harnais laisse rose
titre : Harnais avec laisse rose pour chats
description: Harnais avec fermetures clip et laisse 1,20 m pour chats.
image: "/assets/img/boutique/Accessoires/harnais-laisse-rose.jpg"
order: 2
---
