---
name: Cage transport
titre : Capri
description: Cage de transport en platique avec poignée. Fentes de ventilations sur les côtés pour une meilleure circulation de l'air.
image: "/assets/img/boutique/Accessoires/cage-transport.jpg"
order: 11
---
