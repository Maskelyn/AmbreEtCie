---
name: Collier elastique
titre : Collier chat, élastique
description: Collier pour chats elastique et réglable, avec clochettes. Disponibles en plusieurs couleurs.
image: "/assets/img/boutique/Accessoires/collier-elastique.jpg"
order: 5
---
