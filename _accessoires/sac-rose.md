---
name: Sac rose
titre : Fina
description: Sac de transport avec poche, poignées et fond rembourré amovibles. Laisse courte à l'intérieur pour plus de sécurité et filet de protection permettant une bonne ciruclation de l'air.
image: "/assets/img/boutique/Accessoires/sac-rose.jpg"
order: 10
---
