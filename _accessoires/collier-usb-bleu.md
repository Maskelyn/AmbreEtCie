---
name: Collier usb bleu  
titre : USB Easy Flash Collier bleu
description: Collier avec fermeture clip eclairante, réglable en modes éclairage clignotant ou continu. Collier en silicone, réglable, robuste et facile d'entretient. Rechargeable par cable usc (inclus).
image: "/assets/img/boutique/Accessoires/collier-usb-bleu.jpg"
order: 7

---
