---
name: Lit radiateur
titre : Lit radiateur
description: Couverture peluche, cadre en métal réglable.
image: "/assets/img/boutique/Accessoires/lit-radiateur.jpg"
order: 15
---
