---
name: Collier usb rose
titre : USB Easy Flash Collier rose
description: Collier avec fermeture clip eclairante, réglable en modes éclairage clignotant ou continu. Collier en silicone, réglable, robuste et facile d'entretient. Rechargeable par cable usc (inclus).
image: "/assets/img/boutique/Accessoires/collier-usb-rose.jpg"
order: 8
---
